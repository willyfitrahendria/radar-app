package com.example.newradar;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.SensorListener;
import android.hardware.SensorManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.AudioManager;
import android.media.ExifInterface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.TextView;
import android.widget.Toast;

@SuppressWarnings("deprecation")
@SuppressLint("NewApi")
public class NewRadar extends Activity {
	public NewRadarView newRadarView;
	private SensorManager mSensorManager;
	private AudioManager audiomanager;
	public static Activity activity;
    private File file;
    private LocationManager locationManager;
    public static double questderajattrack;
    public static String questIDtrack="";
    public static double questdistancetrack;
    public static String questBSSIDtrack="";
    public static int questSignaltrack;
    private static double currentLat;
    private static double currentLong;
    private SubMenu sub;
    private boolean active;
    private  Location location;
    private String provider;
    private Criteria criteria;
    private float mOrientation;
    public static boolean track_mode;
    private float azimuth = 0 ;
	private Context context = this;
    public static List<Quest> ListQuest= new ArrayList<Quest>();
    private MediaPlayer mp;
    private float lastAzimuth = 0 ;
    private String grupid = "4c8b7b4a7cfa9037705b09b28af74b3a";

    
    
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_new_radar);
           
        if (newRadarView==null){
        	newRadarView = (NewRadarView) findViewById(R.id.radar);
        	assert newRadarView != null;}
      
        locationManager=(LocationManager) getSystemService(Context.LOCATION_SERVICE);
        mSensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
        // Get the AudioManager
      audiomanager =(AudioManager)this.getSystemService(Context.AUDIO_SERVICE);
        // Set the volume of played media to your choice.
        
        criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setAltitudeRequired(false);
        criteria.setBearingRequired(false);
        criteria.setCostAllowed(true);
        criteria.setPowerRequirement(Criteria.POWER_LOW);

    }
    
    LocationListener locationListener = new LocationListener(){
    	public void onLocationChanged(Location location){
    		updateWithNewLocation(location);
    	}
    	public void onProviderDisabled(String provider){
    		updateWithNewLocation(null);
    	}
    	public void onProviderEnabled(String provider){}
    	public void onStatusChanged(String provider, int status, Bundle extras){}
    	
    };
    
    SensorListener sensorListener = new SensorListener(){
	    public void onAccuracyChanged(int sensor, int accuracy) {
	    }

	    public void onSensorChanged(int sensor, float[] values) {
	        mOrientation = values[0];
	        TextView myLocationText = (TextView)findViewById(R.id.myLocationText);
	        String latlong = ""+currentLat +"\n" +currentLong+"\n"+mOrientation;
	        myLocationText.setText(latlong);
	        azimuth = (mOrientation+360)%360;
	        if( Math.abs(azimuth- lastAzimuth) > 5){
	        	rotateCompass();
	        }
	        newRadarView.postInvalidate();
	    }
    };
    
    private void updateWithNewLocation(Location location){
    	TextView myLocationText = (TextView)findViewById(R.id.myLocationText);
    	String latlong;
    	if (location!=null){
    		currentLat = location.getLatitude();
    		currentLong = location.getLongitude();
    		latlong = ""+currentLat +"\n" +currentLong+"\n"+mOrientation;
    	}else{
    		latlong = "no location found";
    	}
    	myLocationText.setText(latlong);
    }

    public void rotateCompass(){
    	Animation compassAnim = new RotateAnimation((float)(360-lastAzimuth),(float)(360-azimuth), Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
    	compassAnim.setDuration(1000);
    	compassAnim.setRepeatCount(0);
    	compassAnim.setFillAfter(true);
    	newRadarView.startAnimation(compassAnim);
    	compassAnim=null;
    	lastAzimuth = azimuth;
    }
    
   @Override
   protected void onResume(){
	   Log.d("ONRESUME","ONRESUME");
	   provider = locationManager.getBestProvider(criteria, true);
	   if (provider!=null){
		   location=locationManager.getLastKnownLocation(provider);
		   updateWithNewLocation(location);
		   locationManager.requestLocationUpdates(provider,2000,10,locationListener);
	   }else{
new AlertDialog.Builder(this)
    .setTitle("GPS OFF")
    .setMessage("Please Turn On GPS Manually")
    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) { 
            // continue with delete
        }
     })
     .show();
		   Toast.makeText(context, "Turn On GPS", Toast.LENGTH_LONG).show();
	   }
	   try{
		   questSignaltrack=getSignalfromBSSID(questBSSIDtrack);
	   }catch(Exception e){
		   e.printStackTrace();
		   questSignaltrack=-500;
		   new AlertDialog.Builder(this)
		    .setTitle("WIFI is OFF")
		    .setMessage("Please Turn On WIFI Manually")
		    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int which) { 
		            // continue with delete
		        }
		     })
		     .show();
		   Toast.makeText(context, "Turn on WIFI", Toast.LENGTH_LONG).show();
	   }
       mp = MediaPlayer.create(this,R.raw.sound);
	   new Thread(new Runnable() {
			public void run() {
				active = true;
				int j=0;
				int k = -1;
				while (active) {
					try {
						if (track_mode){								
							j++;
							questSignaltrack=getSignalfromBSSID(questBSSIDtrack);
							k = (int)-questSignaltrack/5;
								if (j>=k){
									Log.d("MEDIAP","MEDIAP");
									mp.start();
									j=0;
								}
						}						
						newRadarView.postInvalidate();
						
						if (track_mode){
							mSensorManager.registerListener(sensorListener,SensorManager.SENSOR_ORIENTATION,
					        SensorManager.SENSOR_DELAY_GAME);							
						}else{
							if (azimuth!=0 && mOrientation!=0){
								azimuth=0;
								mOrientation=0;
								rotateCompass();							
								mSensorManager.unregisterListener(sensorListener);
							}
						}
				        locationManager.requestLocationUpdates(provider,2000,10,locationListener);
				    	Thread.sleep(200);
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
	   }).start();
	   super.onResume();
   }
   
	@Override
	protected void onPause() {
		Log.d("ONPAUSE","ONPAUSE");
		active = false;
		mSensorManager.unregisterListener(sensorListener);
		locationManager.removeUpdates(locationListener);
		super.onPause();
	}
	
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_new_radar, menu);
        sub = menu.addSubMenu(0,1,0, "Tracking Mode");
        for (int i= 0;i<=ListQuest.size()-1;i++){ 
        	sub.add(0,i,0,ListQuest.get(i).getID());
        }
        Log.d("UHUHU","l");
        return true;
    }
    
    @Override
    public boolean onPrepareOptionsMenu(Menu menu){
    	Log.d("onPREPAREOPTIONMENU","onPREPAREOPTIONMENU");
       sub.clear();
        for (int i= 0;i<=ListQuest.size()-1;i++){    
        	sub.add(0,i,0,""+(i+1)+". "+ListQuest.get(i).getID());
        }
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	 for (int i = 0 ; i <= ListQuest.size()-1;i++){ 	  //handle tracking mode 
	    	   if(item.getTitle().equals(""+(i+1)+". "+ListQuest.get(i).getID())) {
	    		   questderajattrack=ListQuest.get(i).getDEG();
	    		   Log.d("DERAJAT TRACK",""+questderajattrack);
	    		   questdistancetrack=ListQuest.get(i).getDIST();
	    		   questIDtrack=ListQuest.get(i).getID();
	    		   questBSSIDtrack=ListQuest.get(i).getBSSID();
	    		   track_mode=true;
	    		   return true;
	    	   }
  	    }
       if(item.getTitle().equals("Tracking Mode")) {
    	   Log.d("UHUHU","Tracking Mode is Pressed");
       }else if(item.getTitle().equals("Radar Mode")){
    	    Log.d("RADAR MODE", "RADAR MODE");    
      		RadarMode();
      		return true;
       }else if (item.getTitle().equals("Refresh")){
    	   	Log.d("REFRESH","REFRESH");
    	   	RefreshMode();
    	   	return true;
       }else if (item.getTitle().equals("Camera Mode")){ //karena pakai intent, resolusi set manual di device (camera depan)
    	   Log.d("CAMERA MODE","CAMEREA MODE");
    	   CameraMode();
    	   return true;
       }else if (item.getTitle().equals("Reset")){
    	   	Log.d("RESET","RESET");
    	   	ResetMode();
			return true;
       }else if (item.getTitle().equals("Exit")){
    	    ExitMode();
       }
       Log.d("KAKAKA",""+item.getTitle());
       return super.onOptionsItemSelected(item);
    }    
        
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	 if (requestCode == 100 && resultCode == RESULT_CANCELED ) {
             Toast toast = Toast.makeText(this,"Canceled, no picture taken.", Toast.LENGTH_LONG);
             toast.show();
             return;
         }else
        	if (requestCode == 100 &&resultCode == RESULT_OK) {
                if (requestCode == 100) 
                {  
                	
                	Log.d("CAMERA INTENT", "Camera intent return");
                    ResizeResolution(file);
                	geoTag(file.getAbsolutePath(),currentLat,currentLong);
                	AcquireChest(file);
                }
        	}
     }
        
        
     public void AcquireChest(File file){
    	  HttpClient httpClient = new DefaultHttpClient();
      	  HttpPost httpPost = new HttpPost("http://milestone.if.itb.ac.id/pbd/index.php");
  			try {
  				  MultipartEntity entity = new MultipartEntity();
  		    	  entity.addPart("group_id", new StringBody(grupid));
  		    	  entity.addPart("chest_id", new StringBody(questIDtrack));
  		    	  entity.addPart("bssid", new StringBody(questBSSIDtrack));
  		    	  entity.addPart("action", new StringBody("acquire"));
  		    	  entity.addPart("wifi", new StringBody(""+questSignaltrack));
  		    	  entity.addPart("file", new FileBody(file));
  		    	 
  		    	  httpPost.setEntity(entity);
  				try {
  					HttpResponse httpResponse = httpClient.execute(httpPost);
  					String responacquire = EntityUtils.toString(httpResponse.getEntity()/*, Encoding */);
  					Log.d("RESPON acquire", ""+responacquire);
  					Toast.makeText(context, responacquire, Toast.LENGTH_LONG).show();
  					JSONObject jObj;
  					try{
  						jObj = new JSONObject(responacquire);
  						if (jObj.getString("status").equals("success")){
  							new AlertDialog.Builder(this)
  						    .setTitle("Acquire Chest")
  						    .setMessage("Acquiring Chest ID : "+questIDtrack+" is Success")
  						    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
  						        public void onClick(DialogInterface dialog, int which) { 
  						            // continue with delete
  						        }
  						     })
  						     .show();
  							RadarMode();
  						}else{
  							new AlertDialog.Builder(this)
  						    .setTitle("Acquire Chest")
  						    .setMessage("Failed : "+jObj.getString("description"))
  						    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
  						        public void onClick(DialogInterface dialog, int which) { 
  						            // continue with delete
  						        }
  						     })
  						     .show();  							
  						}
  					}catch (Exception e){
  						e.printStackTrace();
  					}
  				} catch (ClientProtocolException cpe) {
  					System.out.println("First Exception caz of HttpResponese :" + cpe);
  					cpe.printStackTrace();
  				} catch (IOException ioe) {
  					System.out.println("Second Exception caz of HttpResponse :" + ioe);
  					ioe.printStackTrace();
  				}
  			} catch (UnsupportedEncodingException uee) {
  				System.out.println("An Exception given because of UrlEncodedFormEntity argument :" + uee);
  				uee.printStackTrace();
  			}
        }
     
        public int getSignalfromBSSID(String bssid) {
        	WifiManager wifiManager = (WifiManager)getSystemService(Context.WIFI_SERVICE);
        	boolean BSSIDWifiOk=false;
        	
            List<ScanResult> results = wifiManager.getScanResults();
            if (results==null){
            	
            }
            int i = 0;
            for (i=0; i<results.size(); i++)
            {    	
            	 if (bssid.equals(results.get(i).BSSID)){
            		 BSSIDWifiOk=true;
            		 break;            		
            	 }
            }
            if(BSSIDWifiOk){
            	return results.get(i).level;
            }else{
            	return -500;
            }
        }
        
        public void ParserResponseRet(String respon){
        	JSONObject jObj;
        	JSONArray jObj2;
            try {
                jObj = new JSONObject(respon);
                ListQuest.clear();
                if (!jObj.getString("data").equals("null")){
                	jObj2 =	jObj.getJSONArray("data");
                	ListQuest.clear();
                	for (int i = 0; i < jObj2.length(); i++) {
                		JSONObject rec = jObj2.getJSONObject(i);
                		String id = rec.getString("id");
                		String bssid = rec.getString("bssid");
                		Double distance = rec.getDouble("distance");
                		Double degree = rec.getDouble("degree");
                		ListQuest.add(new Quest(id,bssid,distance,degree));
                    
                	}
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        
        public void geoTag(String filename, double latitude, double longitude){
            ExifInterface exif;
            String NS;
            String EW;
            if (latitude<0){
            	latitude=-latitude;
            	NS = "S";
            }else{
            	NS = "N";
        	}
            
            if (longitude<0){
            	longitude=-longitude;
            	EW = "W";
            }else{
            	EW = "E";
        	}
            
            try {
                exif = new ExifInterface(filename);
                int num1Lat = (int)Math.floor(latitude);
                int num2Lat = (int)Math.floor((latitude - num1Lat) * 60);
                double num3Lat = (latitude - ((double)num1Lat+((double)num2Lat/60))) * 3600000;

                int num1Lon = (int)Math.floor(longitude);
                int num2Lon = (int)Math.floor((longitude - num1Lon) * 60);
                double num3Lon = (longitude - ((double)num1Lon+((double)num2Lon/60))) * 3600000;

                exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE, num1Lat+"/1,"+num2Lat+"/1,"+num3Lat+"/1000");
                exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE, num1Lon+"/1,"+num2Lon+"/1,"+num3Lon+"/1000");
                exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE_REF, NS); 
                exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF, EW);    
               
                exif.saveAttributes();
            } catch (IOException e) {
                Log.e("PictureActivity", e.getLocalizedMessage());
            } 

       };
       
       public void RadarMode(){
    	   try{
  		       	HttpGet get = new HttpGet();
  				get.setURI(new URI("http://milestone.if.itb.ac.id/pbd/index.php?group_id="+grupid+"&action=retrieve&latitude="+currentLat+"&longitude="+currentLong));
  				HttpClient client = new DefaultHttpClient();
  			    HttpResponse response = client.execute(get);
  			    String responretrieve = EntityUtils.toString(response.getEntity()/*, Encoding */);
  				Log.d("RESPON", ""+responretrieve);
  				ParserResponseRet(responretrieve);
  	   			Toast.makeText(context, responretrieve, Toast.LENGTH_LONG).show();
     		}catch(Exception e){
     			Log.d("LOL", "POP");
     		}      	
     		track_mode=false;
       }
       
       public void RefreshMode(){
    	   try{
		       	HttpGet get = new HttpGet();
				get.setURI(new URI("http://milestone.if.itb.ac.id/pbd/index.php?group_id="+grupid+"&action=retrieve&latitude="+currentLat+"&longitude="+currentLong));
				HttpClient client = new DefaultHttpClient();
			    HttpResponse response = client.execute(get);
			    String responretrieve = EntityUtils.toString(response.getEntity()/*, Encoding */);
				Log.d("RESPON", ""+responretrieve);
				Toast.makeText(context, responretrieve, Toast.LENGTH_LONG).show();
				ParserResponseRet(responretrieve);
				if (track_mode){
					for(int i = 0 ; i<=ListQuest.size()-1;i++){
						if (questIDtrack.equals(ListQuest.get(i).getID())){
							questdistancetrack=ListQuest.get(i).getDIST();
							questderajattrack=ListQuest.get(i).getDEG();
						}
					}
				}
    	   	}catch(Exception e){
    	   		new AlertDialog.Builder(this)
				    .setTitle("Internet is OFF")
				    .setMessage("Please Turn On Internet Manually")
				    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
				        public void onClick(DialogInterface dialog, int which) { 
				            // continue with delete
				        }
				     })
				     .show();
    	   		Toast.makeText(context, "Turn On Internet", Toast.LENGTH_LONG).show();
    	   		e.printStackTrace();
    	   	}
       }

       public void CameraMode(){
    	   String path=Environment.getExternalStorageDirectory().getPath() + "/DCIM/Camera/";
    	   file = new File(path,"test.jpg");
    	   try {
    		   file.createNewFile();
    	   } catch (IOException e) {
    		   e.printStackTrace();
    	   }
    	   Uri outputFileUri = Uri.fromFile(file);
    	   Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
    	   intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
    	   startActivityForResult(intent,100);
       }
       
       public void ResetMode(){
    	   new AlertDialog.Builder(this)
		    .setTitle("Reset")
		    .setMessage("Are You Sure?")
		    .setPositiveButton("YES", new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int which) { 
	     	   
		    	  	HttpClient httpClient = new DefaultHttpClient();
					HttpPost httpPost = new HttpPost("http://milestone.if.itb.ac.id/pbd/index.php");
					
					BasicNameValuePair groupidBasicNameValuePair = new BasicNameValuePair("group_id", grupid);
					BasicNameValuePair actionBasicNameValuePAir = new BasicNameValuePair("action", "reset");

					List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
					nameValuePairList.add(groupidBasicNameValuePair);
					nameValuePairList.add(actionBasicNameValuePAir);
					try {
						UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(nameValuePairList);
						httpPost.setEntity(urlEncodedFormEntity);
						try {
							HttpResponse httpResponse = httpClient.execute(httpPost);
							String responreset = EntityUtils.toString(httpResponse.getEntity()/*, Encoding */);
							Log.d("RESPON2", ""+responreset);
							Toast.makeText(context, responreset, Toast.LENGTH_LONG).show();
						} catch (ClientProtocolException cpe) {
							System.out.println("First Exception caz of HttpResponese :" + cpe);
							cpe.printStackTrace();
						} catch (IOException ioe) {
							System.out.println("Second Exception caz of HttpResponse :" + ioe);
							ioe.printStackTrace();
						}

					} catch (UnsupportedEncodingException uee) {
						System.out.println("An Exception given because of UrlEncodedFormEntity argument :" + uee);
						uee.printStackTrace();
					}
				 	RefreshMode();
				 	
		        }
		        
		     })
		     
		     .setNegativeButton("NO", new DialogInterface.OnClickListener() {
			        public void onClick(DialogInterface dialog, int which) { 
			            // continue with delete
			        }
			     })
		     .show();
    	   
    	   
    	   
    	   
       }
       
       public void ExitMode(){
    		new AlertDialog.Builder(this)
			    .setTitle("Exit")
			    .setMessage("Are You Sure?")
			    .setPositiveButton("YES", new DialogInterface.OnClickListener() {
			        public void onClick(DialogInterface dialog, int which) { 
			        		finish();
			        		Intent intent = new Intent(Intent.ACTION_MAIN);
			    	    	intent.addCategory(Intent.CATEGORY_HOME);
			    	    	intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			    	    	startActivity(intent);
			    	    	android.os.Process.killProcess(android.os.Process.myPid());
			        }
			     })
			     
			     .setNegativeButton("NO", new DialogInterface.OnClickListener() {
				        public void onClick(DialogInterface dialog, int which) { 
				            // continue with delete
				        }
				     })
			     .show();
    	   
       }
       
       public void ResizeResolution(File file){
    	   Bitmap scaledphoto = null;
           int height = 640;
           int width = 480;    
           String path=Environment.getExternalStorageDirectory().getPath() + "/DCIM/Camera/test.jpg";
           Bitmap photo = BitmapFactory.decodeFile( path );
           Log.d("FETCHED", "Picture fetched");
           scaledphoto = Bitmap.createScaledBitmap(photo, width, height, true);
           Log.d("SCALED", "Picture scaled");
          
           OutputStream fOut = null;
           try {
				fOut = new FileOutputStream(file);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
           scaledphoto.compress(Bitmap.CompressFormat.JPEG, 90, fOut);
           Log.d("SAVED", "Scaled picture saved");
       }
       
       public void getNumberChest(){
    	   try{
    		   	HttpGet get2 = new HttpGet();
	   			get2.setURI(new URI("http://milestone.if.itb.ac.id/pbd/index.php?group_id="+grupid+"&action=number"));
	   			HttpClient client2 = new DefaultHttpClient();
	   			HttpResponse response2 = client2.execute(get2);
	   			String responnumber = EntityUtils.toString(response2.getEntity()/*, Encoding */);
	   			Log.d("RESPON", ""+responnumber);
	   			Toast.makeText(context, responnumber, Toast.LENGTH_LONG).show();
    	   }catch(Exception e){
    		   e.printStackTrace();
    	   }
       }
       
       @Override
       public boolean onKeyDown(int keyCode, KeyEvent event) {
           switch (keyCode) {
           case KeyEvent.KEYCODE_VOLUME_UP:
               audiomanager.adjustStreamVolume(AudioManager.STREAM_MUSIC,
                       AudioManager.ADJUST_RAISE, AudioManager.FLAG_SHOW_UI);
               return true;
           case KeyEvent.KEYCODE_VOLUME_DOWN:
               audiomanager.adjustStreamVolume(AudioManager.STREAM_MUSIC,
                       AudioManager.ADJUST_LOWER, AudioManager.FLAG_SHOW_UI);
               return true;
           case KeyEvent.KEYCODE_BACK :
                   onPause(); 
                   this.finish();
        	   return true;
           default:
               return false;
           }
       }
      
       
}
