package com.example.newradar;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.util.AttributeSet;
import android.view.View;

public class NewRadarView extends View {
	private Paint mGridPaint;
	private Paint paint;
	private Paint wpaint;
	public static int center;
    public static int radius;
	    
	 public NewRadarView(Context context) {
		 this(context, null);
	 }
	    
	 public NewRadarView(Context context, AttributeSet attrs) {
		 this(context, attrs, 0);
	 }
	    
	 public NewRadarView(Context context, AttributeSet attrs, int defStyle) {
	        super(context, attrs, defStyle);
	        
	        mGridPaint = new Paint(); //green paint
	        mGridPaint.setColor(Color.GREEN);
	        mGridPaint.setAntiAlias(true);
	        mGridPaint.setStyle(Style.STROKE);
	        mGridPaint.setStrokeWidth(1.0f);
	        mGridPaint.setTextSize(20.0f);
	        mGridPaint.setTextAlign(Align.CENTER);
	        
	        paint = new Paint(); //red paint
	        paint.setColor(Color.RED); 
	        paint.setAntiAlias(true);
	        paint.setStyle(Style.STROKE);
	        paint.setStrokeWidth(1.0f);
	        paint.setTextSize(20.0f);
	        paint.setTextAlign(Align.CENTER);
	        
	        wpaint = new Paint(); // white paint
	        wpaint.setColor(Color.WHITE); 
	        wpaint.setAntiAlias(true);
	        wpaint.setStyle(Style.STROKE);
	        wpaint.setStrokeWidth(1.0f);
	        wpaint.setTextSize(20.0f);
	        wpaint.setTextAlign(Align.CENTER);
	 }
	 
	 @Override
	 protected void onDraw(Canvas canvas) {
	        super.onDraw(canvas);
	        center = getWidth()/2;
	        radius = center - 10;
	     
	        canvas.drawColor(Color.BLACK);
	        final Paint gridPaint = mGridPaint;
	        final Paint redPaint = paint;
	        final Paint whitepaint = wpaint;
	        canvas.drawCircle(center, center, radius, gridPaint); //draw circle ring
	        canvas.drawCircle(center, center, 1, gridPaint);
	        canvas.drawCircle(center, center, radius*2/5, gridPaint);
	        canvas.drawCircle(center, center, radius*3/5, gridPaint);
	        canvas.drawCircle(center, center, radius*1/5, gridPaint);
	        canvas.drawCircle(center, center, radius*4/5, gridPaint);
	       
	        canvas.drawLine((float) (center+(radius*1/5)*Math.sin(Math.toRadians(90))), (float) (center-(radius*1/5)*Math.cos(Math.toRadians(90))), (float)(center+radius*Math.sin(Math.toRadians(90))),(float)(center-radius*Math.cos(Math.toRadians(90))), gridPaint);
	        canvas.drawLine((float) (center+(radius*1/5)*Math.sin(Math.toRadians(270))), (float) (center-(radius*1/5)*Math.cos(Math.toRadians(270))), (float)(center+radius*Math.sin(Math.toRadians(270))),(float)(center-radius*Math.cos(Math.toRadians(270))), gridPaint);
	        canvas.drawLine((float) (center+(radius*1/5)*Math.sin(Math.toRadians(0))), (float) (center-(radius*1/5)*Math.cos(Math.toRadians(0))), (float)(center+radius*Math.sin(Math.toRadians(0))),(float)(center-radius*Math.cos(Math.toRadians(0))), gridPaint);
	        canvas.drawLine((float) (center+(radius*1/5)*Math.sin(Math.toRadians(180))), (float) (center-(radius*1/5)*Math.cos(Math.toRadians(180))), (float)(center+radius*Math.sin(Math.toRadians(180))),(float)(center-radius*Math.cos(Math.toRadians(180))), gridPaint);
	      
	        canvas.drawLine((float) (center+(radius*1/5)*Math.sin(Math.toRadians(135))), (float) (center-(radius*1/5)*Math.cos(Math.toRadians(135))), (float)(center+radius*Math.sin(Math.toRadians(135))),(float)(center-radius*Math.cos(Math.toRadians(135))), gridPaint);
	        canvas.drawLine((float) (center+(radius*1/5)*Math.sin(Math.toRadians(315))), (float) (center-(radius*1/5)*Math.cos(Math.toRadians(315))), (float)(center+radius*Math.sin(Math.toRadians(315))),(float)(center-radius*Math.cos(Math.toRadians(315))), gridPaint);
	        canvas.drawLine((float) (center+(radius*1/5)*Math.sin(Math.toRadians(45))), (float) (center-(radius*1/5)*Math.cos(Math.toRadians(45))), (float)(center+radius*Math.sin(Math.toRadians(45))),(float)(center-radius*Math.cos(Math.toRadians(45))), gridPaint);
	        canvas.drawLine((float) (center+(radius*1/5)*Math.sin(Math.toRadians(225))), (float) (center-(radius*1/5)*Math.cos(Math.toRadians(225))), (float)(center+radius*Math.sin(Math.toRadians(225))),(float)(center-radius*Math.cos(Math.toRadians(225))), gridPaint);
	        
	        if (!NewRadar.track_mode){
	        for (int i = 0; i<=NewRadar.ListQuest.size()-1;i++){ //jika respon retrive tidak null, gambar quest di layar
		        double distance = NewRadar.ListQuest.get(i).getDIST()*(1.5); // dalam pixel dikali 1.5
		        double questderajat = NewRadar.ListQuest.get(i).getDEG();
		        if (distance<=radius){
		        	canvas.drawCircle((float)(center+distance*Math.sin(Math.toRadians(questderajat))),(float)(center-distance*Math.cos(Math.toRadians(questderajat))),2, redPaint);
		        	canvas.drawText(""+(i+1), (float)(center+distance*Math.sin(Math.toRadians(questderajat))), (float)(center-5-distance*Math.cos(Math.toRadians(questderajat))), whitepaint);
		        }
	        }
	      }
	     
	        if (NewRadar.track_mode){ //semakin besar kekuatan sinyal maka garis (panah) semakin panjang
	        	int panjanggaris = 0;
	        	panjanggaris=radius+NewRadar.questSignaltrack;
	        	if (panjanggaris<10){
	        		panjanggaris=10;
	        	}else if(panjanggaris>150){
	        		panjanggaris=150;
	        	}
	        		canvas.drawLine(center, center, (float)(center+panjanggaris*Math.sin(Math.toRadians(NewRadar.questderajattrack))),(float)(center-panjanggaris*Math.cos(Math.toRadians(NewRadar.questderajattrack))), redPaint);
	        }
	       
	        canvas.drawText("U", center, center-radius, redPaint);
	 }
}
