package com.example.newradar;

public class Quest {
	private String id;
	private String bssid;
	private double distance;
	private double degrees;
	
	public Quest(String id_,String bssid_,double distance_,double degrees_){
		id=id_;
		bssid=bssid_;
		distance=distance_;
		degrees=degrees_;
		
	}
	public String getID(){
		return id;
	}
	public String getBSSID(){
		return bssid;
	}
	public double getDIST(){
		return distance;
	}
	public double getDEG(){
		return degrees;
	}
	
} 
